from odoo import fields, models, api


class ResPartner(models.Model):
    _inherit = 'res.partner'
    _order = 'name'
    authored_book_ids = fields.Many2many(
        'library.book', string='Authored Books')
    count_books = fields.Integer('Number of Authored Books ', compute='_compute_count_books')
    published_book_ids = fields.One2many(
        'library.book', 'publisher_id',
        string='Published Books')

    @api.depends("authored_book_ids")
    def _compute_count_books(self):
        for partner in self:
            partner.count_books = len(partner.authored_book_ids)
