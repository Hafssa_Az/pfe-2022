from odoo import fields, models, api


class LibraryMember(models.Model):
    _name = 'library.member'
    _inherits = {'res.partner':'partner_id'}
    _description = 'Description'

    name = fields.Char()
    partner_id = fields.Many2one('res.partner',
                                 ondelete='cascade')
    date_start = fields.Date('Mamber Since')
    date_end = fields.Date('Termination Date')
    member_number = fields.Char('')
    date_of_birth = fields.Dtae('Dae of birth')

