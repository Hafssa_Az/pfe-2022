from odoo import models, fields, api
from datetime import timedelta
from odoo.exceptions import ValidationError


class LibraryBook(models.Model):
    _name = 'library.book'
    _description = 'Library Book'
    _order = 'date_release desc,name'

    name = fields.Char('Title',
                       required=True)
    parent_book_id = fields.Many2one('library.book')
    pages_parent_book = fields.Integer('Pages', related='parent_book_id.pages')
    author_ids = fields.Many2many('res.partner', string='Authors')
    cost_price = fields.Float(
        'Book Cost', digits='Book Price')
    retail_price = fields.Monetary(
        'Retail Price',
        # optional: currency_field='currency_id',
    )
    currency_id = fields.Many2one(
        'res.currency', string='Currency')

    short_name = fields.Char('Short Title')
    notes = fields.Text('Internal Notes')
    state = fields.Selection(
        [('draft', 'Not Available'),
         ('available', 'Available'),
         ('lost', 'Lost')],
        string='State', default='draft')
    description = fields.Html('Description')
    cover = fields.Binary('Book Cover')
    out_of_print = fields.Boolean('Out of Print?')
    date_release = fields.Date('Release Date')
    date_updated = fields.Datetime('Last Updated')
    company_id = fields.Many2one('res.company')

    """
    context is a dictionary load values of record which set by default
    {
    'defaul_field_name': value
    }
    """
    """
    states apply on model when your model has a field called state 
    
    {
    'state_value': [('readonly or required or invisible ', True or false)],
    'draft': [('required', True), ('readonly', False)]
    }
    
    {
    'available': [('readonly', True), ('invisible', False)],
    ''
    
    }
    
    """
    pages = fields.Integer('Number of Pages',
                           groups='base.group_user',
                           # states={'draft': [('readonly', True)]},
                           help='Total book page count', company_dependent=False)
    reader_rating = fields.Float(
        'Reader Average Rating',
        digits=(14, 4),  # Optional precision decimals,
    )

    """ ondelete has value of three 
        1- cascade if delete then delete every records linked with it
        2- set null if delete then set value null 
        3- restrict if delete then stop user from delete this record                       
    """
    publisher_id = fields.Many2one(
        'res.partner', string='Publisher',
        ondelete='set null',
        context={},
        domain=[],
    )

    publisher_city = fields.Char(
        'Publisher City', related='publisher_id.city')

    author_ids = fields.Many2many(
        'res.partner', string='Authors')
    category_id = fields.Many2one('library.book.category')
    age_days = fields.Float(
        string='Days Since Release',
        compute='_compute_age',
        inverse='_inverse_age',
        search='_search_age',
        store=False,  # optional
        compute_sudo=True  # optional
    )

    def name_get(self):
        result = []
        for record in self:
            rec_name = "%s (%s)" % (record.name, record.cost_price)
            result.append((record.id, rec_name))
        return result

    @api.constrains('name')
    def _check_name(self):
        books = self.search([('id', '!=', self.id)])
        for book in books:
            if book.name == self.name:
                raise models.ValidationError('Alreadey exist !')

    @api.constrains('date_release')
    def _check_release_date(self):
        for record in self:
            if record.date_release and record.date_release > fields.Date.today():
                raise models.ValidationError('Release date must be in the past')

    @api.depends('date_release')
    def _compute_age(self):
        print("In Compute")
        today = fields.Date.today()

        for book in self:
            if book.date_release:
                delta = today - book.date_release
                book.age_days = delta.days
            else:
                book.age_days = 0

    def _inverse_age(self):
        today = fields.Date.today()
        for book in self.filtered('date_release'):
            d = today - timedelta(days=book.age_days)
            book.date_release = d

    def action_draft(self):
        self.state = 'draft'

    def action_available(self):
        print('Available')
        self.sudo().write({'state': 'available'})

    def action_lost(self):
        print('Last')
        self.state = 'lost'

    # for record in self:


#     names = self.search([('id', '!=', self.id)]).mapped('name')
#     print("Names ", names)
#     if self.name in names:
#         raise ValidationError('This name exists')

# if record.date_release and record.date_release > fields.Date.today():
#     raise models.ValidationError(
#         'Release date must be in the past')

    @api.model
    def create(self, vals):
        # print('vals',vals)
        print('context', self._context, self.env.context)
        result = super().create(vals)
        print('result',result)
        return result

    @api.depends('cost_price')
    @api.depends_context('company_id')
    def _compute_value(self):
        company_id = self.env.context.get('company_id')

