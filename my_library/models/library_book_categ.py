from odoo import fields, models, api
from odoo.exceptions import ValidationError


class BookCategory(models.Model):
    _name = 'library.book.category'

    name = fields.Char()
    parent_id = fields.Many2one(
        'library.book.category',
        string='Parent Category',
        ondelete='restrict',
        index=True)
    child_ids = fields.One2many('library.book.category', 'parent_id', string='Child Category')
    parent_store = True
    parent_path = fields.Char(index=True)
    _parent_name = 'parent_id'

    @api.constrains('parent_id')
    def _check_hierarchy(self):
        if not self._check_recursion():
            raise models.ValidationError('Error! You cannot create recursive categories.')

        
