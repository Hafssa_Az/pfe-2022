{

    'name': "My Library",
    'summary': "Manage books easily",
    'depends': ['base', 'purchase'],
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/library_book.xml',
        'views/library_book.categ.xml'
        # 'views/res_partner_view.xml'
    ],

}
